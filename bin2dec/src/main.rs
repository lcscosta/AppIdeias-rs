use std::env;
use std::process;

fn verify(binary: String) -> bool {
    for c in binary.chars(){
        if c.ne(&'0') & c.ne(&'1'){ 
            return false;
        }
    }
    return true;
}

fn bin2dcm(binary: String) -> i32 {

    let mut decimal:i32 = 0;

    for (i, c) in binary.chars().rev().enumerate(){
        let n:i32 = c as i32 - 0x30;
        decimal += n * 2_i32.pow(i.try_into().unwrap())
    }
    return decimal
}

fn main() {
    // Input string like '010101011'
    let args: Vec<String> = env::args().collect();
    
    // verify if string have only 0 or 1
    let binary = args[1].to_string();
    
    //let _verify = verify(binary);
    if verify(binary.clone()) == false{
        println!("Insert an valid binary argument.");
        process::exit(1);
    };

    let dcm = bin2dcm(binary.clone());

    println!("{}", dcm);
    //println!("{:?}", args[1]);
}
